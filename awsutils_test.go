package awsutils

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGenerateProxyResponse(t *testing.T) {
	tables := []struct {
		body       string
		statusCode int
		wantpr     events.APIGatewayProxyResponse
		wanter     error
	}{
		{"Hello, world!", 200, events.APIGatewayProxyResponse{Body: "Hello, world!", StatusCode: 200}, nil},
	}

	for _, table := range tables {
		gotpr, goter := GenerateProxyResponse(table.body, table.statusCode)
		assert.Equal(t, table.wantpr, gotpr)
		assert.Equal(t, table.wanter, goter)
	}
}

func TestBuildErrorJson(t *testing.T) {
	msg := "Missing request body"
	errCode := 400

	want := "{\"error\":{\"code\":400,\"message\":\"Missing request body\"}}"
	got := BuildErrorJson(msg, errCode)

	assert.Equal(t, want, got)
}
