package awsutils

import (
	"github.com/aws/aws-lambda-go/events"
	"log"
	"strconv"
)

// GenerateProxyResponse generates an AWS API Gateway Proxy Response based on an HTTP body and status code
func GenerateProxyResponse(body string, statusCode int) (events.APIGatewayProxyResponse, error) {
	log.Printf("Debug: ++GenerateProxyResponse()")
	defer log.Printf("Debug: --GenerateProxyResponse()")

	log.Printf("RESPONSE: " + body)

	return events.APIGatewayProxyResponse{
		Body:       body,
		StatusCode: statusCode,
	}, nil
}

// BuildErrorJson formats a JSON string including an error message and HTTP error code.
func BuildErrorJson(errMsg string, errCode int) string {
	log.Printf("Debug: ++BuildErrorJson()")
	defer log.Printf("Debug: --BuildErrorJson()")
	return "{\"error\":{\"code\":" + strconv.Itoa(errCode) + ",\"message\":\"" + errMsg + "\"}}"
}
